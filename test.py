
# coding: UTF-8
import urllib.request, urllib.error
from bs4 import BeautifulSoup

def scraping_function(url):
    
    def Scraping_wiki(url):

        # アクセスするURL
        #url = "https://ja.wikipedia.org/wiki/%E7%97%85%E6%B0%97"

        # URLにアクセスする　HTMLが変え当て来
        html = urllib.request.urlopen(url)

        # htmlをBeautifulSoupで扱う
        soup = BeautifulSoup(html, "html.parser")

        # 
        div = soup.find_all("div")

        wiki_list = []

        for div_tag in div:
        
            #print(div_tag)
            try:

                string_ = div_tag.get("class").pop(0)
                #print(div_tag.get("class"),"配列")
                #print(string_)
                p_tag = ""
                if string_ in "mw-body-content":

                    p = div_tag.find_all("p")
                    for p_tag in p:
                        #print(p_tag)

                        #print(p_tag, 'p_tag')
                        #print(p_tag.text)
                        try:
                            wiki_list.append(p_tag.text)
                        except:
                            pass
                    #break
            except:
                pass
        #print(wiki_list)

        print(wiki_list)
        return wiki_list,soup

    wiki_list,soup = Scraping_wiki(url)

    #wiki_list = ''.join(wiki_list)

    f = open('{}.txt'.format(soup.title.string), 'w',  encoding='UTF-8')
    for x in wiki_list:
        f.write(str(x) + "\n")
    f.close()


