import wx
import os

import sys
sys.path.append('/path/to/dir')
import re

import test


class MyForm(wx.Frame):
    # ----------------------------------------------------------------------
    def __init__(self):
        wx.Frame.__init__(self, None, wx.ID_ANY, size=(600, 200))
        root_panel = wx.Panel(self, wx.ID_ANY)
        root_layout = wx.BoxSizer(wx.VERTICAL)

        hbox1 = wx.BoxSizer(wx.HORIZONTAL)
        self.tc = wx.TextCtrl(root_panel)

        hbox1.Add(self.tc, 1, wx.RIGHT, 8)
        #ボタン作成

        # スクレイピング
        ocr_bu = wx.Button(root_panel, wx.ID_ANY, "スクレイピング")
        ocr_bu.Bind(wx.EVT_BUTTON, self.change_txt)

        hbox1.Add(ocr_bu, 0)

        root_layout.Add(hbox1, 0, wx.EXPAND | wx.LEFT | wx.RIGHT | wx.TOP, 10)
        root_panel.SetSizer(root_layout)
        # ----------------------------------------------------------------------

    def change_txt(self,event):
        '''
        入力されたURLからスクレイピングを行い、テキストファイルとして保存する。
        '''
        filename = self.tc.GetValue()
        print(filename)
        try:
            test.scraping_function(filename)
        finally:
            wx.MessageBox(u'txtファイルとして保存完了しました。', u'保存完了')

# ----------------------------------------------------------------------
# Run the program
if __name__ == "__main__":
    app = wx.App(False)
    frame = MyForm()
    frame.Show()
    app.MainLoop()

